public class Board{
	final int GAMEOVER=3;
	private Die die1;
	private Die die2;
	private int[][] closedTiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new int[6][6];
	}
	public String toString(){
		String filler = "";
		for(int x=0; x < closedTiles.length; x++){
			for(int y=0; y < closedTiles[x].length; y++){
			filler = filler + closedTiles[x][y];
			}
			filler = filler + " ";
			
		}
		return filler;
	}
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		
		if(closedTiles[die1.getPips()-1][die2.getPips()-1]==GAMEOVER-1){
			return true;
			}
		else{
		  closedTiles[die1.getPips()-1][die2.getPips()-1] = closedTiles[die1.getPips()-1][die2.getPips()-1]+1;
		  return false;
		}
	}
}