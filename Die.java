import java.util.Random;
public class Die{
	private int pips;
	private Random rad;
	
	public Die(){
		this.pips = 1;
		this.rad = new Random();
	}
	public int getPips(){
		return this.pips;
	}
	public void roll(){
		this.pips=rad.nextInt(6)+1;
	}
	public String toString(){
		return ""+getPips();
	}
}